import datetime

import pandas as pd
from assertpy import assert_that
from google.cloud.bigquery import Client

from sql_test_kit import (
    Column,
)
from sql_test_kit.bigquery import BigqueryTable, BigQueryInterpolator
from sql_test_kit.query_interpolation import QueryInterpolator


def test_bigquery_query_interpolation():
    # Given
    sales_amount_col = "SALES_AMOUNT"
    sales_date_col = "SALES_DATE"
    sales_table = BigqueryTable(
        project="project",
        dataset="dataset",
        table="table",
        columns=[
            Column(sales_amount_col, "FLOAT64"),
            Column(sales_date_col, "STRING"),
        ],
    )
    current_year_sales_by_day_query = f"""
        SELECT {sales_date_col}, SUM({sales_amount_col}) as {sales_amount_col}
        FROM {sales_table}
        WHERE {sales_date_col} >= "2023-01-01"
        GROUP BY {sales_date_col}
    """
    sales_data = pd.DataFrame(
        {
            "SALES_ID": [1, 2, 3, 4],
            sales_date_col: ["2022-12-31", "2023-01-01", "2023-01-01", "2023-01-02"],
            sales_amount_col: [10, 20, 30, 40],
        }
    )

    # When
    interpolated_query = QueryInterpolator() \
        .add_input_table(sales_table, sales_data) \
        .interpolate_query(current_year_sales_by_day_query)
    current_year_sales_by_day_data = Client().query(interpolated_query).to_dataframe()

    # Then
    expected_current_year_sales_by_day_data = pd.DataFrame(
        {
            sales_date_col: ["2023-01-01", "2023-01-02"],
            sales_amount_col: [50, 40],
        }
    )

    pd.testing.assert_frame_equal(
        current_year_sales_by_day_data,
        expected_current_year_sales_by_day_data,
        check_dtype=False,
    )


def test_bigquery_query_interpolation_with_null_dates():
    # Given
    sales_amount_col = "SALES_AMOUNT"
    sales_date_col = "SALES_DATE"
    sales_table = BigqueryTable(
        project="project",
        dataset="dataset",
        table="table",
        columns=[
            Column(sales_amount_col, "FLOAT64"),
            Column(sales_date_col, "DATE"),
        ],
    )
    current_year_sales_by_day_query = f"""
        SELECT {sales_date_col}, SUM({sales_amount_col}) as {sales_amount_col}
        FROM {sales_table}
        WHERE {sales_date_col} >= "2023-01-01"
        GROUP BY {sales_date_col}
    """
    sales_data = pd.DataFrame(
        {
            "SALES_ID": [1, 2, 3, 4],
            sales_date_col: ["2022-12-31", "2023-01-01", "2023-01-02", None],
            sales_amount_col: [10, 20, 30, 40],
        }
    )

    # When
    interpolated_query = BigQueryInterpolator() \
        .add_input_table(sales_table, sales_data) \
        .interpolate_query(current_year_sales_by_day_query)
    current_year_sales_by_day_data = Client().query(interpolated_query).to_dataframe()

    # Then
    expected_current_year_sales_by_day_data = pd.DataFrame(
        {
            sales_date_col: [datetime.date(2023, 1, 1), datetime.date(2023, 1, 2)],
            sales_amount_col: [20, 30],
        }
    )

    pd.testing.assert_frame_equal(
        current_year_sales_by_day_data,
        expected_current_year_sales_by_day_data,
        check_dtype=False,
    )


def test_bigquery_table_path():
    # Given
    bigquery_table = BigqueryTable(
        project="project",
        dataset="dataset",
        table="table",
        columns=[],
    )

    # When
    table_path = bigquery_table.table_path

    # Then
    assert_that(table_path).is_equal_to("`project.dataset.table`")


def test_bigquery_table_string_representation():
    # Given
    bigquery_table = BigqueryTable(
        project="project",
        dataset="dataset",
        table="table",
        columns=[],
    )

    # When
    table_string_representation = str(bigquery_table)

    # Then
    assert_that(table_string_representation).is_equal_to("`project.dataset.table`")
